FROM ubuntu:focal-20221019

RUN DEBIAN_FRONTEND=noninteractive apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends -o DPkg::options::="--force-confdef" -o DPkg::options::="--force-confold" build-essential libgl-dev cmake ninja-build python3-pip \
    && DEBIAN_FRONTEND=noninteractive apt-get clean -y \
    && pip3 install --no-cache-dir conan \
    && rm -rf /var/cache/debconf/* \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /usr/share/doc/* \
    && rm -rf /usr/share/man/* \
    && rm -rf /usr/share/local/* \
    && rm -rf /var/cache/debconf/* \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/log/* \
    && rm -rf /tmp/* \
    && rm -rf /var/tmp/*
